package stopwatch;

/**
 * Sum 100,000,000 double values from an array.
 * @author Pipatpol	Tanvavongchinda
 * @version 2015-01-29
 */
public class Task3 implements Runnable{
	/** The loop counter used in the tasks. */
	private int counter;
	/** The size of array. */
	static final int ARRAY_SIZE = 500000;
	/** Create array of double. */
	double[] values  = new double[ARRAY_SIZE];
	
	/**
	 * Initialize Task3.
	 * @param counter is the loop counter.
	 */
	public Task3 (int counter)
	{
		this.counter = counter;
		for(int k=0; k<ARRAY_SIZE; k++) values[k] = k+1;
	}
	
	/**
	 * Summation of this array.
	 */
	public void run()
	{
		double sum = 0.0;
		// count = loop counter, i = array index value
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum + values[i];
		}
		System.out.println("sum = " + sum);
	}
	
	/**
	 * Describe this task what is it doing.
	 * @return Tell what does this task do.
	 */
	public String toString()
	{
		return String.format("Sum array of double primitives with count=%,d\n", counter);
	}
}
