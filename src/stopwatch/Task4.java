package stopwatch;

/**
 * Sum 100,000,000 Double objects.
 * @author Pipatpol	Tanvavongchinda
 * @version 2015-01-29
 */
public class Task4 implements Runnable{
	/** The loop counter used in the tasks. */
	private int counter;
	/** The size of array. */
	static final int ARRAY_SIZE = 500000;
	/** Create array of Double. */
	Double[] values = new Double[ARRAY_SIZE];
	
	/**
	 * Initialize Task4.
	 * @param counter is the loop counter.
	 */
	public Task4(int counter)
	{
		
		for(int i=0; i<ARRAY_SIZE; i++) values[i] = new Double(i+1);
		this.counter = counter;
	}
	
	/**
	 * Summation of this array.
	 */
	public void run()
	{
		Double sum = new Double(0.0);
		// count = loop counter, i = array index
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum + values[i];
		}
		System.out.println("sum = " + sum);
	}
	
	/**
	 * Describe this task what is it doing.
	 * @return Tell what does this task do.
	 */
	public String toString()
	{
		return String.format("Sum array of Double objects with count=%,d\n", counter);
	}
	
}
