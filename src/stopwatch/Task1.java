package stopwatch;

/**
 * Append 100,000 characters to a String. Display length of the result.
 * @author Pipatpol	Tanvavongchinda
 * @version 2015-01-29
 */
public class Task1 implements Runnable{
	/** The loop counter used in the tasks. */
	private int counter;
	/** A char that add to String. */
	final char CHAR = 'a';
	
	/**
	 * Initialize Task1.
	 * @param counter is the loop counter.
	 */
	public Task1 (int counter)  
	{
		this.counter = counter;
	}
	
	/**
	 * Add a character counter times.
	 */
	public void run()
	{
		String sum = ""; 
		int k = 0;
		while(k++ < counter) {
			sum = sum + CHAR;
		}
		System.out.println("final string length = " + sum.length());
	}
	
	/**
	 * Describe this task what is it doing.
	 * @return Tell what does this task do.
	 */
	public String toString()
	{
		return String.format("Append to String with count=%,d\n",counter);
	}
}
